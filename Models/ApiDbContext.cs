using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;

namespace WebApi.Models
{
	public class ApiDbContext : DbContext
	{
		public ApiDbContext(DbContextOptions<ApiDbContext> options) : base(options)
		{
		}

		public DbSet<Message> Messages { get; set; }
		public DbSet<Forum> Forums { get; set; }
		public DbSet<Topic> Topics { get; set; }
		public DbSet<Post> Posts { get; set; }
		public DbSet<BadWord> BadWords { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			base.OnConfiguring(optionsBuilder);

			var factory = new LoggerFactory();
			factory.AddProvider(new NLogLoggerProvider());
			optionsBuilder.UseLoggerFactory(factory);
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);

			builder.Entity<Message>().HasKey(__message => __message.Id);
			builder.Entity<Message>().Property(__message => __message.Text).IsRequired();
			builder.Entity<Message>().Property(__message => __message.CreateDate).IsRequired();
			builder.Entity<Message>().HasOne(__message => __message.Question);

			//Forum
			builder.Entity<Forum>().HasKey(__forum => __forum.Id);
			builder.Entity<Forum>().Property(__forum => __forum.Name).IsRequired();
			builder.Entity<Forum>().HasMany(__forum => __forum.Topics);

			//Topic
			builder.Entity<Topic>().HasKey(__topic => __topic.Id);
			builder.Entity<Topic>().Property(__topic => __topic.Name).IsRequired();
			builder.Entity<Topic>().HasOne(__topic => __topic.Forum);
			builder.Entity<Topic>().HasMany(__topic => __topic.Posts);

			//Post
			builder.Entity<Post>().HasKey(__post => __post.Id);
			builder.Entity<Post>().Property(__post => __post.Message).IsRequired();
			builder.Entity<Post>().HasOne(__post => __post.Topic);

			//BadWord
			builder.Entity<BadWord>().HasKey(__word => __word.Id);
			builder.Entity<BadWord>().Property(__word => __word.Word).IsRequired();
		}
	}
}