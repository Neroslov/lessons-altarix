﻿using System;

namespace WebApi.Models
{
	public class Post :ModelId
	{

		public string Message { get; set; } 

		public Topic Topic { get; set; }
		public Guid? TopicId { get; set; }

		public string Autor { get; set; }

	}
}