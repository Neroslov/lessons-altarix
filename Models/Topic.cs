﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
	public class Topic : ModelId
	{
		public string Name { get; set; }
		
		public Guid? ForumId { get; set; }
		public Forum Forum { get; set; }

		public string Autor { get; set; }

		public ICollection<Post> Posts { get; set; }
	}
}