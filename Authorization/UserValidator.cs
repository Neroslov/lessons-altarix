﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;

using Nancy.Authentication.Basic;

namespace WebApi.Authorization
{
	public class UserValidator : IUserValidator
	{
		public ClaimsPrincipal Validate(string username, string password)
		{
			if(username == "admin" && password == "admin")
			{
				//return new ClaimsPrincipal(new ClaimsIdentity(username));
				var NewUser = new GenericIdentity(username);
				NewUser.AddClaims(new Claim[] { new Claim(ClaimTypes.Name, username), new Claim(ClaimTypes.Role, "admin")});

				return new ClaimsPrincipal(NewUser);
			}
			if(username == "user" && password == "user")
			{
				var NewUser = new GenericIdentity(username);
				NewUser.AddClaims(new Claim[] { new Claim(ClaimTypes.Name, username), new Claim(ClaimTypes.Role, "user") });

				return new ClaimsPrincipal(NewUser);
			}
			

			return null;
		}
	}
}
