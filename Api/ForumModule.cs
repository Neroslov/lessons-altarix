﻿using System;
using Microsoft.EntityFrameworkCore;
using Nancy.ModelBinding;
using WebApi.Models;
using Nancy.Security;
using System.Security.Claims;

namespace WebApi.Api
{
	public sealed class ForumModule : ApiModuleBase
	{
		public ForumModule(ApiDbContext context) : base("/forum")
		{
			Get("/", name: "GetAllForums", action: async (__, __token) =>
			{
				this.RequiresAuthentication();

				var forums = await context.Forums.ToListAsync(__token).ConfigureAwait(false);
				return forums;
			});

			Get("/{id}", name: "GetForumById", action: async (__params, __token) =>
			{
				this.RequiresAuthentication();

				Guid id = __params.Id;

				var forum = await context.Forums.Include(__forum => __forum.Topics)
												.FirstOrDefaultAsync(__forum => __forum.Id == id, __token)
												.ConfigureAwait(false);

				return forum;
			});

			Post("/", name: "AddForum", action: async (__, __token) =>
			{
				this.RequiresAuthentication();

				Forum forum = this.Bind();
				if (await context.BadWords.FirstOrDefaultAsync(__word => forum.Name.ToLower().Contains(__word.Word.ToLower())) != null) return "Swearing!";


				await context.AddAsync(forum, __token).ConfigureAwait(false);
				await context.SaveChangesAsync(__token).ConfigureAwait(false);
				return forum;
			});

			Post("/edit/{id}/", name: "EditForum", action: async (__params, __token) =>
			{
				this.RequiresClaims(__claim => __claim.Type == ClaimTypes.Role && __claim.Value == "admin");

				Guid id = __params.Id;

				var forum = await context.Forums.FirstOrDefaultAsync(__forum => __forum.Id == id).ConfigureAwait(false);
				forum.Name = ((Topic)this.Bind()).Name;

				if (await context.BadWords.FirstOrDefaultAsync(__word => forum.Name.ToLower().Contains(__word.Word.ToLower())) != null) return "Swearing!";


				await context.SaveChangesAsync(__token).ConfigureAwait(false);
				return forum;
			}); 

			Get("/del/{id}", name: "DelForum", action: async (__params, __token) =>
			{
				this.RequiresClaims(__claim => __claim.Type == ClaimTypes.Role && __claim.Value == "admin");

				Guid id = __params.Id;

				var forum = await context.Forums.FirstOrDefaultAsync(__forum => __forum.Id == id).ConfigureAwait(false);
				context.Entry(forum).State = EntityState.Deleted;

				await context.SaveChangesAsync(__token).ConfigureAwait(false);
				return "Удалено";
			});
		}

	}
}

