using System;
using Microsoft.EntityFrameworkCore;
using Nancy.ModelBinding;
using WebApi.Models;
using Nancy.Security;
using System.Text.RegularExpressions;
using System.Security.Claims;

namespace WebApi.Api
{
	public sealed class MessagesModule : ApiModuleBase
	{
		public MessagesModule(ApiDbContext context) : base("/messages")
		{
			Get("/", name: "GetAllMessages", action: async (__, __token) =>
			{
				this.RequiresAuthentication();

				var messages = await context.Messages.ToListAsync(__token).ConfigureAwait(false);

				return messages;
			});

			Get("/{id}", name: "GetMessageById", action: async (__params, __token) =>
			{
				this.RequiresAuthentication();
				Guid id = __params.Id;

				var message = await context.Messages.FirstOrDefaultAsync(__message => __message.Id == id, __token).ConfigureAwait(false);

				return message;
			});

			Post("/{questionId}/", name: "AddResponse", action: async (__params, __token) =>
			{
				this.RequiresAuthentication();

				Guid id = __params.questionId;
				Message response = this.Bind();

				response.CreateDate = DateTimeOffset.UtcNow;
				response.Question = await context.Messages.SingleAsync(__message => __message.Id == id, __token).ConfigureAwait(false);

				await context.AddAsync(response, __token).ConfigureAwait(false);

				await context.SaveChangesAsync(__token).ConfigureAwait(false);

				return response;
			});

			Post("/", name: "AddQuestion", action: async (__, __token) =>
			{
				this.RequiresAuthentication();

				Message question = this.Bind();

				if (await context.BadWords.FirstOrDefaultAsync(__word => question.Text.ToLower().Contains(__word.Word.ToLower())) != null) return "Swearing!"; 

				question.CreateDate = DateTimeOffset.UtcNow;

				await context.AddAsync(question, __token).ConfigureAwait(false);
				await context.SaveChangesAsync(__token).ConfigureAwait(false);

				return question;
			});

			Post("/edit/{id}/", name: "EditMessage", action: async (__params, __token) =>
			{
				this.RequiresClaims(__claim => __claim.Type == ClaimTypes.Role && __claim.Value == "admin");

				Guid id = __params.Id;
				var message = await context.Messages.FirstOrDefaultAsync(__message => __message.Id == id).ConfigureAwait(false);
				message.Text = ((Message)this.Bind()).Text;

				await context.SaveChangesAsync(__token).ConfigureAwait(false);
				return message;
			});

			Get("/del/{id}", name: "DelMessage", action: async (__params, __token) =>
			{
				this.RequiresClaims(__claim => __claim.Type == ClaimTypes.Role && __claim.Value == "admin");

				Guid id = __params.Id;

				var message = await context.Messages.FirstOrDefaultAsync(__message => __message.Id == id).ConfigureAwait(false);
				context.Entry(message).State = EntityState.Deleted;
				await context.SaveChangesAsync(__token).ConfigureAwait(false);

				return "�������";
			});
		}
	}
}