﻿using System;
using Microsoft.EntityFrameworkCore;
using Nancy.ModelBinding;
using WebApi.Models;
using Nancy.Security;
using System.Security.Claims;

namespace WebApi.Api
{
	public class PostModule : ApiModuleBase
	{
		public PostModule(ApiDbContext context) : base("/posts")
		{
			Get("/", name: "GetAllPosts", action: async (__, __token) =>
			{
				this.RequiresAuthentication();

				var posts = await context.Posts.ToListAsync(__token).ConfigureAwait(false);
				return posts;
			});

			Get("/{id}", name: "GetPostById", action: async (__params, __token) =>
			{
				this.RequiresAuthentication();

				Guid id = __params.Id;

				var post = await context.Posts .FirstOrDefaultAsync(__post=> __post.Id == id, __token)
												.ConfigureAwait(false);

				return post;
			});


			Post("/", name: "AddPost", action: async (__, __token) =>
			{
				this.RequiresAuthentication();

				Post post = this.Bind();
				if (await context.BadWords.FirstOrDefaultAsync(__word => post.Message.ToLower().Contains(__word.Word.ToLower())) != null) return "Swearing!";


				post.Autor = this.Context.CurrentUser.Identity.Name;
				await context.AddAsync(post, __token).ConfigureAwait(false);

				await context.SaveChangesAsync(__token).ConfigureAwait(false);
				return post;
			});

			Post("/edit/{id}/", name: "EditPost", action: async (__params, __token) =>
			{
				this.RequiresClaims(__claim => __claim.Type == ClaimTypes.Role && __claim.Value == "admin");

				Guid id = __params.Id;
				var post = await context.Posts.FirstOrDefaultAsync(__post => __post.Id == id).ConfigureAwait(false);
				post.Message= ((Post)this.Bind()).Message;

				if (await context.BadWords.FirstOrDefaultAsync(__word => post.Message.ToLower().Contains(__word.Word.ToLower())) != null) return "Swearing!";

				await context.SaveChangesAsync(__token).ConfigureAwait(false);
				return post;
			});

			Get("/del/{id}", name: "DelPost", action: async (__params, __token) =>
			{
				this.RequiresClaims(__claim => __claim.Type == ClaimTypes.Role && __claim.Value == "admin");

				Guid id = __params.Id;

				var post = await context.Posts.FirstOrDefaultAsync(__post => __post.Id == id).ConfigureAwait(false);
				context.Entry(post).State = EntityState.Deleted;

				await context.SaveChangesAsync(__token).ConfigureAwait(false);
				return "Удалено";
			});
		}

	}
}

