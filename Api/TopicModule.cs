﻿using System;
using Microsoft.EntityFrameworkCore;
using Nancy.ModelBinding;
using WebApi.Models;
using Nancy.Security;
using System.Security.Claims;

namespace WebApi.Api
{
	public class TopicModule : ApiModuleBase
	{
		public TopicModule(ApiDbContext context) : base("/topics")
		{
			Get("/", name: "GetAllTopics", action: async (__, __token) =>
			{
				this.RequiresAuthentication();

				var topics = await context.Topics.ToListAsync(__token).ConfigureAwait(false);
				return topics;
			});

			Get("/{id}", name: "GetTopicById", action: async (__params, __token) =>
			{
				this.RequiresAuthentication();

				Guid id = __params.Id;

				var topic = await context.Topics.Include(__topic => __topic.Posts)
												.FirstOrDefaultAsync(__topic => __topic.Id == id, __token)
												.ConfigureAwait(false);

				return topic;
			});

			Post("/", name: "AddTopic", action: async (__, __token) =>
			{
				this.RequiresAuthentication();

				Topic topic = this.Bind();
				if (await context.BadWords.FirstOrDefaultAsync(__word => topic.Name.ToLower().Contains(__word.Word.ToLower())) != null) return "Swearing!";


				topic.Autor = this.Context.CurrentUser.Identity.Name;
				await context.AddAsync(topic, __token).ConfigureAwait(false);
				await context.SaveChangesAsync(__token).ConfigureAwait(false);
				return topic;
			});

			Post("/edit/{id}/", name: "EditTopic", action: async (__params, __token) =>
			{
				this.RequiresClaims(__claim => __claim.Type == ClaimTypes.Role && __claim.Value == "admin");

				Guid id = __params.Id;
				var topic = await context.Topics.FirstOrDefaultAsync(__topic => __topic.Id == id).ConfigureAwait(false);
				topic.Name = ((Topic)this.Bind()).Name;

				if (await context.BadWords.FirstOrDefaultAsync(__word => topic.Name.ToLower().Contains(__word.Word.ToLower())) != null) return "Swearing!";

				await context.SaveChangesAsync(__token).ConfigureAwait(false);
				return topic;
			});

			Get("/del/{id}", name: "DelTopic", action: async (__params, __token) =>
			{
				this.RequiresClaims(__claim => __claim.Type == ClaimTypes.Role && __claim.Value == "admin");

				Guid id = __params.Id;

				var topic = await context.Topics.FirstOrDefaultAsync(__topic => __topic.Id == id).ConfigureAwait(false);
				context.Entry(topic).State = EntityState.Deleted;

				await context.SaveChangesAsync(__token).ConfigureAwait(false);
				return "Удалено";
			});
		}

	}
}

